/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arbol;

/**
 *
 * @author Cristian Berzunza 
 */
public class Arbol {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int dato;
        String nombre;
        ArbolBinario miArbol = new ArbolBinario();
        miArbol.agregarNodo(79, "O");
        miArbol.agregarNodo(82, "R");
        miArbol.agregarNodo(76, "L");
        miArbol.agregarNodo(65, "A");
        miArbol.agregarNodo(78, "N");
        miArbol.agregarNodo(68, "D");
        miArbol.agregarNodo(73, "I");
        miArbol.agregarNodo(69, "E");
        miArbol.agregarNodo(67, "C");
        miArbol.agregarNodo(72, "H");
        miArbol.agregarNodo(83, "S");
        
        
        System.out.println("PreOrden");
        if (!miArbol.estaVacio()){
            miArbol.preOrden(miArbol.raiz);
        }
        
        
    }
    
}
